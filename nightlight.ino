// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
    #include <avr/power.h>
#endif
#include "button.h"

// NeoPixel pin
#define PIXELS_PIN  0
#define REDLED_PIN 1
#define BUTTON_PIN 2
// Number of NeoPixels
#define NUMPIXELS  3
#define INTENSITY_DELTA 2

// loop period in milliseconds
#define LOOP_PERIOD 100
// Instantiate pixel control class
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIXELS_PIN, NEO_GRB + NEO_KHZ800);
// auto-off time in seconds
#define AUTO_OFF_SEC (30*60)

#define NUMCOLORS 8
// night light color
uint32_t nlcolor[NUMCOLORS] =
{
    0xffffff,
    0xffff00,
    0x00ffff,
    0xff00ff,
    0x45ab45,
    0x0000ff,
    0x00ff00,
    0xff0000,
};

enum LightMode {
    NightLight,
    IntensitySetup,
    };

enum LightState {
    LightsOff, // Initial state
    DeviceOn, // White night light
    };

void setup() {
    // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
    // End of trinket special code

    pinMode(BUTTON_PIN, INPUT);
    pinMode(REDLED_PIN, OUTPUT);
    pixels.begin(); // This initializes the NeoPixel library.
}

void setColorOfAllPixels(int r, int g, int b){
    for(int i=0;i<NUMPIXELS;i++){
        pixels.setPixelColor(i, pixels.Color(r,g,b));
        pixels.show(); // This sends the updated pixel color to the hardware.
    }
}

bool buttonClicked(button_t &btn)
{
    return btn.state == Released && btn.timer < 500;
}

button_t updateButton(button_t btn)
{
    btn = button_update(btn, !digitalRead(BUTTON_PIN));
    if(buttonClicked(btn))
        digitalWrite(REDLED_PIN, HIGH);
    else if(btn.state == Pressed && btn.timer > 1500)
        digitalWrite(REDLED_PIN, HIGH);
    else
        digitalWrite(REDLED_PIN, LOW);
    return btn;
}

void set_rgb(unsigned int *buf, const uint32_t rgb)
{
    buf[0] = (rgb >> 16) & 0xff;
    buf[1] = (rgb >> 8 ) & 0xff;
    buf[2] = (rgb >> 0 ) & 0xff;
}

// color component intensity
#define COMP_INT(x, i) (((x * i)/100) & 0xff)

uint32_t set_intensity(uint32_t rgb, uint16_t intensity)
{
    uint16_t r,g,b;
    uint32_t ret;

    r = (rgb >> 16) & 0xff;
    g = (rgb >> 8 ) & 0xff;
    b = (rgb >> 0 ) & 0xff;

    r = COMP_INT(r, intensity);
    g = COMP_INT(g, intensity);
    b = COMP_INT(b, intensity);

    return ((uint32_t)(r) << 16) | g << 8 | b;
}

void loop() {
    // initial state
    static LightState state = LightsOff;
    static LightMode mode = NightLight;
    // rgb color 
    static unsigned int rgbColor[3] = {0, 0, 0};
    // button state
    static button_t btn = { false, 0, 0, NotPressed };
    // intensity, 0-100
    static unsigned char intensity = 30;
    // color array index
    static unsigned char cidx = 0;
    // time in seconds lights have been on
    static uint16_t nightlight_idle_s = 0;
    static unsigned long nightlight_start_ms = 0;

    unsigned long t = millis();

    btn = updateButton(btn);

    uint8_t r,g,b;
    switch(state)
    {
        case LightsOff:
            set_rgb(rgbColor, 0);

            if(buttonClicked(btn))
            {
                mode = NightLight;
                state = DeviceOn;
                nightlight_idle_s = 0; // reset "on" time
                nightlight_start_ms = t;
            }
            else if(btn.state == Pressed && btn.timer > 1500)
            {
                state = DeviceOn;
                mode = IntensitySetup;
            }
            break;

        case DeviceOn:
            switch(mode)
            {
                case NightLight:
                    // auto-off timer
                    if((t - nightlight_start_ms) > 1000)
                    {
                        // increment second timer
                        nightlight_idle_s++;
                        // reset millisecond start
                        nightlight_start_ms = t;
                    }

                    if(buttonClicked(btn))
                    {
                        cidx = (cidx + 1) % NUMCOLORS;
                        nightlight_idle_s = 0; // reset idle timer
                    }
                    else if(btn.state == Pressed && btn.timer > 1500)
                    {
                        // don' change state to off until button is released
                        set_rgb(rgbColor, set_intensity(0, 0));
                    }
                    else if(btn.state == Released && btn.timer > 1500)
                    {
                        // on release after long press, turn off
                        state = LightsOff;
                    }
                    else if(nightlight_idle_s > AUTO_OFF_SEC)
                    {
                        state = LightsOff;
                    }
                    else
                    {
                        set_rgb(rgbColor, set_intensity(nlcolor[cidx], intensity));
                    }

                    break;
                case IntensitySetup:
                    if(buttonClicked(btn))
                    {
                        state = LightsOff;
                        set_rgb(rgbColor, 0xffffff);
                    }
                    else
                    {
                        if(btn.state == Pressed && btn.timer > 1500)
                        {
                            if(intensity < INTENSITY_DELTA)
                            {
                                intensity = 100;
                            }
                            intensity -= INTENSITY_DELTA;
                        }
                        set_rgb(rgbColor, set_intensity(nlcolor[cidx], intensity));
                    }

                    break;
                default:
                    break;
            }

            break;

        default:
            state = LightsOff;
            break;
    }


    setColorOfAllPixels(rgbColor[0], rgbColor[1], rgbColor[2]);

    unsigned long telapsed = millis() - t;
    if(telapsed < LOOP_PERIOD)
        delay(LOOP_PERIOD-telapsed); // Delay for a period of time (in milliseconds).

}

